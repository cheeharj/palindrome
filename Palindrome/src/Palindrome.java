
public class Palindrome {

	public static void main(String[] args) {
//		System.out.println("Palindrome test for the word \"Anna\": " + Palindrome.isPalindrome("Anna"));
//		System.out.println("Palindrome test for the word \"Annaa\": " + Palindrome.isPalindrome("Annaa"));

	}
	
	public static boolean isPalindrome(String input)
	{
		input = input.toLowerCase().replace(" ", "");
		
		for(int i = 0, j = input.length() - 1; i<j; i++, j--)
		{
			if(input.charAt(i) != input.charAt(j))
			{				
				return false;
			}
		}
		
		return true;
		
	}

}
